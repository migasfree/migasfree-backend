# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-07 12:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('client', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Capability',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(blank=True, verbose_name='name')),
                ('description', models.TextField(blank=True, null=True, verbose_name='description')),
            ],
            options={
                'verbose_name': 'Hardware Capability',
                'verbose_name_plural': 'Hardware Capabilities',
            },
        ),
        migrations.CreateModel(
            name='Configuration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(blank=True, verbose_name='name')),
                ('value', models.TextField(blank=True, null=True, verbose_name='value')),
            ],
            options={
                'verbose_name': 'Hardware Configuration',
                'verbose_name_plural': 'Hardware Configurations',
            },
        ),
        migrations.CreateModel(
            name='LogicalName',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(blank=True, verbose_name='name')),
            ],
            options={
                'verbose_name': 'Hardware Logical Name',
                'verbose_name_plural': 'Hardware Logical Names',
            },
        ),
        migrations.CreateModel(
            name='Node',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('level', models.IntegerField(verbose_name='level')),
                ('width', models.IntegerField(null=True, verbose_name='width')),
                ('name', models.TextField(blank=True, verbose_name='id')),
                ('class_name', models.TextField(blank=True, verbose_name='class')),
                ('enabled', models.BooleanField(default=False, verbose_name='enabled')),
                ('claimed', models.BooleanField(default=False, verbose_name='claimed')),
                ('description', models.TextField(blank=True, null=True, verbose_name='description')),
                ('vendor', models.TextField(blank=True, null=True, verbose_name='vendor')),
                ('product', models.TextField(blank=True, null=True, verbose_name='product')),
                ('version', models.TextField(blank=True, null=True, verbose_name='version')),
                ('serial', models.TextField(blank=True, null=True, verbose_name='serial')),
                ('bus_info', models.TextField(blank=True, null=True, verbose_name='bus info')),
                ('physid', models.TextField(blank=True, null=True, verbose_name='physid')),
                ('slot', models.TextField(blank=True, null=True, verbose_name='slot')),
                ('size', models.BigIntegerField(null=True, verbose_name='size')),
                ('capacity', models.BigIntegerField(null=True, verbose_name='capacity')),
                ('clock', models.BigIntegerField(null=True, verbose_name='clock')),
                ('dev', models.TextField(blank=True, null=True, verbose_name='dev')),
                ('computer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='client.Computer', verbose_name='computer')),
                ('parent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='child', to='hardware.Node', verbose_name='parent')),
            ],
            options={
                'verbose_name': 'Hardware Node',
                'verbose_name_plural': 'Hardware Nodes',
            },
        ),
        migrations.AddField(
            model_name='logicalname',
            name='node',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hardware.Node', verbose_name='hardware node'),
        ),
        migrations.AddField(
            model_name='configuration',
            name='node',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hardware.Node', verbose_name='hardware node'),
        ),
        migrations.AddField(
            model_name='capability',
            name='node',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hardware.Node', verbose_name='hardware node'),
        ),
        migrations.AlterUniqueTogether(
            name='configuration',
            unique_together=set([('name', 'node')]),
        ),
        migrations.AlterUniqueTogether(
            name='capability',
            unique_together=set([('name', 'node')]),
        ),
    ]
