# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-07 12:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Connection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='name')),
                ('fields', models.CharField(blank=True, help_text='Fields separated by comma', max_length=100, null=True, verbose_name='fields')),
            ],
            options={
                'verbose_name': 'Connection',
                'verbose_name_plural': 'Connections',
            },
        ),
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, null=True, unique=True, verbose_name='name')),
                ('data', models.TextField(default=b'{}', null=True, verbose_name='data')),
                ('available_for_attributes', models.ManyToManyField(blank=True, to='core.Attribute', verbose_name='available for attributes')),
                ('connection', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='device.Connection', verbose_name='connection')),
            ],
            options={
                'verbose_name': 'Device',
                'verbose_name_plural': 'Devices',
            },
        ),
        migrations.CreateModel(
            name='Driver',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=100, null=True, verbose_name='name')),
                ('packages_to_install', models.TextField(blank=True, null=True, verbose_name='packages to install')),
            ],
            options={
                'ordering': ['model', 'name'],
                'verbose_name': 'Driver',
                'verbose_name_plural': 'Drivers',
            },
        ),
        migrations.CreateModel(
            name='Feature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='name')),
            ],
            options={
                'verbose_name': 'Feature',
                'verbose_name_plural': 'Features',
            },
        ),
        migrations.CreateModel(
            name='Logical',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('alternative_feature_name', models.CharField(blank=True, max_length=50, null=True, verbose_name='alternative feature name')),
                ('attributes', models.ManyToManyField(blank=True, help_text='Assigned Attributes', to='core.Attribute', verbose_name='attributes')),
                ('device', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='device.Device', verbose_name='device')),
                ('feature', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='device.Feature', verbose_name='feature')),
            ],
            options={
                'verbose_name': 'Device Logical',
                'verbose_name_plural': 'Devices Logical',
            },
        ),
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='name')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Manufacturer',
                'verbose_name_plural': 'Manufacturers',
            },
        ),
        migrations.CreateModel(
            name='Model',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, null=True, verbose_name='name')),
                ('connections', models.ManyToManyField(blank=True, to='device.Connection', verbose_name='connections')),
            ],
            options={
                'ordering': ['manufacturer', 'name'],
                'verbose_name': 'Model',
                'verbose_name_plural': 'Models',
            },
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='name')),
            ],
            options={
                'verbose_name': 'Type',
                'verbose_name_plural': 'Types',
            },
        ),
        migrations.AddField(
            model_name='model',
            name='device_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='device.Type', verbose_name='type'),
        ),
        migrations.AddField(
            model_name='model',
            name='manufacturer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='device.Manufacturer', verbose_name='manufacturer'),
        ),
        migrations.AddField(
            model_name='driver',
            name='feature',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='device.Feature', verbose_name='feature'),
        ),
        migrations.AddField(
            model_name='driver',
            name='model',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='device.Model', verbose_name='model'),
        ),
        migrations.AddField(
            model_name='driver',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Project', verbose_name='project'),
        ),
        migrations.AddField(
            model_name='device',
            name='model',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='device.Model', verbose_name='model'),
        ),
        migrations.AddField(
            model_name='connection',
            name='device_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='device.Type', verbose_name='device type'),
        ),
        migrations.AlterUniqueTogether(
            name='model',
            unique_together=set([('device_type', 'manufacturer', 'name')]),
        ),
        migrations.AlterUniqueTogether(
            name='logical',
            unique_together=set([('device', 'feature')]),
        ),
        migrations.AlterUniqueTogether(
            name='driver',
            unique_together=set([('model', 'project', 'feature')]),
        ),
        migrations.AlterUniqueTogether(
            name='device',
            unique_together=set([('connection', 'name')]),
        ),
        migrations.AlterUniqueTogether(
            name='connection',
            unique_together=set([('device_type', 'name')]),
        ),
    ]
